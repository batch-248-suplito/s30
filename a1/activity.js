// 2. Note: use {$match: {onSale: true}}
// A. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
    
    {$match: {stock: {$gte: 20}}},
    { $count: "enoughStock" }

]);


// B. Use the average operator to get the average price of fruits onSale per supplier.
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        avg_price: {$avg: {$sum: "$price"}}
    }}
]);


//C. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        max_price: {$max: {$sum: "$price"}}
    }}

]);

//D. Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        min_price: {$min: {$sum: "$price"}}
    }}

]);