db.fruits.insertMany([
    {
        name : "Apple",
        color : "Red",
        stock : 20,
        price: 40,
        supplier_id : 1,
        onSale : true,
        origin: [ "Philippines", "US" ]
    },
    {
        name : "Banana",
        color : "Yellow",
        stock : 15,
        price: 20,
        supplier_id : 2,
        onSale : true,
        origin: [ "Philippines", "Ecuador" ]
    },
    {
        name : "Kiwi",
        color : "Green",
        stock : 25,
        price: 50,
        supplier_id : 1,
        onSale : true,
        origin: [ "US", "China" ]
    },
    {
        name : "Mango",
        color : "Yellow",
        stock : 10,
        price: 120,
        supplier_id : 2,
        onSale : false,
        origin: [ "Philippines", "India" ]
    }
]);

//MA1: find how many fruits are yellow
db.fruits.aggregate([
    
    {$match: {color: "Yellow"}},
    { $count: "Yellow" }

]);
//MA2: find how many fruit stocks that are less than or equal to 10
db.fruits.aggregate([
    
    {$match: {stock: {$lte: 10}}},
    { $count: "fruitStock" }

]);
//MA3: find fruits for each group of supplier that has the maximum stocks
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        max: {$max: {$sum: "$stock"}}
    }},
    {$project: {
        _id: 0,
    }}

]);


//The following query returns fruit that are on sale and groups them by supplier_ID and sums up the stock
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        total: {$sum: "$stock"}
    }}
]);

//projects excluding the supplier_id.
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        total: {$sum: "$stock"}

    }},
    {$project: {
        _id: 0,
    }}
]);

// sort the stock in descending order of total
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {
        _id: "$supplier_id",
        total: {$sum: "$stock"}
    }},
    {$sort: {total: -1}}
]);

//used to separate the array elements of "origin" into new documents. $unwind: "$origin"
db.fruits.aggregate([
    {$unwind: "$origin"}
]);

// Unwind the origin array of each document
// Group the documents by origin and count the kind of fruits  
db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {
        _id: "$origin",
        kinds: {$sum: 1}
    }}
]);